import React from 'react';
import ReactDOM from 'react-dom';
import { createBrowserHistory } from 'history';
import { Route, Switch, Redirect, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './Reducers';

// core components
import Admin from 'layouts/Admin.js';
import RTL from 'layouts/RTL.js';
import Graph from "views/Graph.js";

import 'assets/css/material-dashboard-react.css?v=1.9.0';

const hist = createBrowserHistory();

const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter history={hist}>
			<Switch>
				<Route path="/admin" component={Admin} />
				<Route path="/rtl" component={RTL} />
				<Route path="/graph" component={Graph} />
				<Redirect from="/" to="/admin/dashboard" />
			</Switch>
		</BrowserRouter>
	</Provider>,
	document.getElementById('root')
);
