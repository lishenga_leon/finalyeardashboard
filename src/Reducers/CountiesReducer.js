import { COUNTIES_FAIL, COUNTIES_SUCCESS, LOADING_STATE, LOADMORE } from '../Actions/types';

const INITIAL_STATE = {
    counties: [],
    loading: false,
    page: 0,
    items: 20,
    loadmore: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case COUNTIES_SUCCESS:
            return {
                ...state,
                counties: action.payload,
                loading: false,
                page: action.page,
                items: action.items,
                loadmore: action.loadmore
            }

        case LOADING_STATE:
            return {
                ...state,
                loading: action.payload,
            }

        case LOADMORE:
            return {
                ...state,
                loadmore: action.payload,
            }

        case COUNTIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
                loadmore: false
            }

        default:
            return state;
    }
}
