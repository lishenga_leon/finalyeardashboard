import { DEVICES_FAIL, DEVICES_SUCCESS, LOADING_STATE, LOADMORE, DEVICESDATA_GOTTEN, DEVICESDATA_NOT } from '../Actions/types';

const INITIAL_STATE = {
    devices: [],
    loading: false,
    page: 0,
    items: 20,
    loadmore: false,
    deviceData: [],
    aqi25: [], 
    aqi10: [], 
    minutes: [], 
    mqSensorCO: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case DEVICES_SUCCESS:

            return {
                ...state,
                devices: action.payload,
                loading: false,
                page: action.page,
                items: action.items,
                loadmore: action.loadmore
            }

        case LOADING_STATE:
            return {
                ...state,
                loading: action.payload,
            }

        case LOADMORE:
            return {
                ...state,
                loadmore: action.payload,
            }

        case DEVICESDATA_GOTTEN:
            return {
                ...state,
                aqi25: action.aqi25, 
                aqi10: action.aqi10, 
                minutes: action.minutes,
                mqSensorCO: action.mqSensorCO
            }

        case DEVICESDATA_NOT:
            return {
                ...state,
            }

        case DEVICES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
                loadmore: false
            }

        default:
            return state;
    }
}
