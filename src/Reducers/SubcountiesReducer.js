import { SUBCOUNTIES_FAIL, SUBCOUNTIES_SUCCESS, LOADING_STATE, LOADMORE } from '../Actions/types';

const INITIAL_STATE = {
    subCounties: [],
    loading: false,
    page: 0,
    items: 20,
    loadmore: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {

        case SUBCOUNTIES_SUCCESS:

            return {
                ...state,
                subCounties: action.payload,
                loading: false,
                page: action.page,
                items: action.items,
                loadmore: action.loadmore
            }

        case LOADING_STATE:
            return {
                ...state,
                loading: action.payload,
            }

        case LOADMORE:
            return {
                ...state,
                loadmore: action.payload,
            }

        case SUBCOUNTIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
                loadmore: false
            }

        default:
            return state;
    }
}
