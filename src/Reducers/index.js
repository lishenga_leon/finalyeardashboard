import { combineReducers } from 'redux';
import CountiesReducer from './CountiesReducer';
import SubcountiesReducer from './SubcountiesReducer';
import DevicesReducer from './DevicesReducer';


export default combineReducers({
    countiesData: CountiesReducer,
    subCountiesData: SubcountiesReducer,
    devicesData: DevicesReducer,
})