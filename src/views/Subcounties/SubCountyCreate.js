import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Button, Form } from 'react-bootstrap';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import 'bootstrap/dist/css/bootstrap.min.css';

const useStyles = makeStyles((theme) => ({
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120,
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
}));

function SubCountyCreate(props) {
	const classes = useStyles();
	const { onHide, createSubCounty, dataCounties } = props;
	const [code, setCode] = useState();
	const [countyId, setCountyId] = useState();
	const [name, setName] = useState();
	const [lat, setLat] = useState();
	const [long, setLong] = useState();
	return (
		<Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">Add SubCounty</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form.Group>
					<Form.Label>Id</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Code" onChange={(event) => { setCode(event.target.value) }} />
					
					<FormControl className={classes.formControl}>
						<InputLabel id="demo-simple-select-label">County</InputLabel>
						<Select
							labelId="demo-simple-select-label"
							id="demo-simple-select"
							onChange={(event) => { 
								setCountyId(event.target.value) }
							}
						>
							{
								dataCounties.map((data) => {
									return (
										<MenuItem value={data.id}>{data.name}</MenuItem>
									)
								})
							}
						</Select>
					</FormControl>
					<br />
					<Form.Label>Name</Form.Label>
					<Form.Control size="sm" type="text" placeholder="Name" onChange={(event) => { setName(event.target.value) }} />
					<br />
					<Form.Label>Latitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Latitude" onChange={(event) => { setLat(event.target.value) }} />
					<br />
					<Form.Label>Longitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Longitude" onChange={(event) => { setLong(event.target.value) }} />
					<br />
				</Form.Group>
			</Modal.Body>
			<Modal.Footer>
				<Button onClick={() => {
					console.log(countyId)
					createSubCounty({ countyId: countyId, name: name, lat: lat, lon: long, });
					onHide()
				}}>Submit</Button>
			</Modal.Footer>
		</Modal>
	);
}

export default SubCountyCreate;
