import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

function Device(props) {
	const { data, onHide, updateDevice } = props;
    const [code, setCode]= useState();
    const [subcountyId, setSubcountyId]= useState();
	const [serialNo, setSerialNo]= useState();
	const [lat, setLat]= useState();
	const [long, setLong]= useState();
	return (
		<Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">Edit {data.name}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form.Group>
					<Form.Label>Code</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Code" defaultValue={data.id} onChange={(event)=>{setCode(event.target.value)}} />
					<br />
                    <Form.Label>County ID</Form.Label>
					<Form.Control size="sm" type="number" placeholder="County ID" onChange={(event)=>{setSubcountyId(event.target.value)}} />
					<br />
					<Form.Label>Name</Form.Label>
					<Form.Control size="sm" type="text" placeholder="Serial Number" defaultValue={data.serialNo} onChange={(event)=>{setSerialNo(event.target.value)}} />
					<br />
					<Form.Label>Latitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Latitude" defaultValue={data.lat} onChange={(event)=>{setLat(event.target.value)}} />
					<br />
					<Form.Label>Longitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Longitude" defaultValue={data.lon} onChange={(event)=>{setLong(event.target.value)}} />
					<br />
				</Form.Group>
			</Modal.Body>
			<Modal.Footer>
				<Button onClick={()=>{
					updateDevice({ deviceId: data.id, subcountyId: subcountyId, serialNo: serialNo, lat: lat, lon: long, latDelta: null, lonDelta: null });
					onHide()
				}}>Submit</Button>
			</Modal.Footer>
		</Modal>
	);
}

export default Device;
