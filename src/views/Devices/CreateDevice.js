import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Modal, Button, Form } from 'react-bootstrap';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';


import 'bootstrap/dist/css/bootstrap.min.css';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

function CreateDevice(props) {
	const classes = useStyles();
    const { onHide, createDevice, dataSubCounties } = props;
    const [code, setCode] = useState();
    const [subcountyId, setSubcountyId] = useState();
    const [serialNo, setSerialNo] = useState();
    const [lat, setLat] = useState();
    const [long, setLong] = useState();
    return (
        <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">Add Device</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group>
                    <FormControl className={classes.formControl}>
                        <InputLabel id="demo-simple-select-label">SubCounty</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            onChange={(event) => {setSubcountyId(event.target.value)}}>
                            {
                                dataSubCounties.map((data) => {
                                    return (
                                        <MenuItem value={data.id}>{data.name}</MenuItem>
                                    )
                                })
                            }
                        </Select>
                    </FormControl>
                    <br />
                    <Form.Label>SerialNo</Form.Label>
                    <Form.Control size="sm" type="text" placeholder="SerialNo" onChange={(event) => { setSerialNo(event.target.value) }} />
                    <br />
                    <Form.Label>Latitude</Form.Label>
                    <Form.Control size="sm" type="number" placeholder="Latitude" onChange={(event) => { setLat(event.target.value) }} />
                    <br />
                    <Form.Label>Longitude</Form.Label>
                    <Form.Control size="sm" type="number" placeholder="Longitude" onChange={(event) => { setLong(event.target.value) }} />
                    <br />
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => {
                    createDevice({ subcountyId: subcountyId, serialNo: serialNo, lat: lat, lon: long, });
                    onHide()
                }}>Submit</Button>
            </Modal.Footer>
        </Modal>
    );
}

export default CreateDevice;
