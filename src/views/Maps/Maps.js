import React from 'react';
import { connect } from 'react-redux';
import { getCountiesServer, getSubcountiesServer, getDevicesServer } from '../../Actions';
import { MapDevicesComponent, MapSubcountiesComponent, MapCountiesComponent } from './Components';

class Maps extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			isMarkerShown: false,
			mapId: null
		};
		this.marker = this.marker.bind(this);
		this.getSubcountiesData = this.getSubcountiesData.bind(this);
		this.getDevicesData = this.getDevicesData.bind(this);
	}
	componentDidMount() {
		this.props.getCountiesServer({ page: 0, items: 20, loadmore: false, counties: this.props.counties });
		this.delayedShowMarker();
	}

	delayedShowMarker = () => {
		setTimeout(() => {
			this.setState({ isMarkerShown: true });
		}, 3000);
	};

	handleMarkerClick = () => {
		this.setState({ isMarkerShown: false });
		this.delayedShowMarker();
	};

	marker(item) {
		this.setState({
			mapId: item.id
		});
	}

	getSubcountiesData(countyId) {
		this.props.getSubcountiesServer({
			page: 0,
			items: 20,
			loadmore: false,
			countyId: countyId,
			subCounties: this.props.subCounties
		});
	}

	getDevicesData(subcountyId) {
		this.props.getDevicesServer({ page: 0, items: 20, loadmore: false, subcountyId: subcountyId });
	}

	render() {
    const { subCounties, counties, devices } = this.props;
     if(subCounties[0] == undefined && devices[0] == undefined ){
      return (
        <MapCountiesComponent
          isMarkerShown={this.state.isMarkerShown}
          onMarkerClick={this.handleMarkerClick}
          counties={counties}
          zoom={7}
          marker={this.marker}
          id={this.state.mapId}
          getSubcountiesData={this.getSubcountiesData}
          getDevicesData={this.getDevicesData}
        />
      );
     }else if(subCounties[0] != undefined && devices[0] == undefined){
      return (
        <MapSubcountiesComponent
          isMarkerShown={this.state.isMarkerShown}
          onMarkerClick={this.handleMarkerClick}
          zoom={12}
          counties={counties}
          subCounties={subCounties}
          marker={this.marker}
          id={this.state.mapId}
          getSubcountiesData={this.getSubcountiesData}
          getDevicesData={this.getDevicesData}
        />
      );
     }else{
      return (
        <MapDevicesComponent
          isMarkerShown={this.state.isMarkerShown}
          onMarkerClick={this.handleMarkerClick}
          zoom={16}
          counties={counties}
          subCounties={subCounties}
          devices={devices}
          marker={this.marker}
          id={this.state.mapId}
          getSubcountiesData={this.getSubcountiesData}
		  getDevicesData={this.getDevicesData}
        />
      );
     }
	}
}

const mapStateToProps = ({ countiesData, subCountiesData, devicesData }) => {
	const { counties, loading, error, loadmore, page, items } = countiesData;
	const { subCounties } = subCountiesData;
	const { devices } = devicesData;

	// const { isRefreshing, reCaller } = commonData;

	return {
		counties,
		loading,
		error,
		loadmore,
		page,
		items,
		subCounties,
		devices
	};
};

export default connect(mapStateToProps, { getCountiesServer, getSubcountiesServer, getDevicesServer })(Maps);