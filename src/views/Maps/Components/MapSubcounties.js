import React from 'react';
import InfoBox from 'react-google-maps/lib/components/addons/InfoBox';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { compose, withProps, withStateHandlers } from 'recompose';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

export const MapSubcountiesComponent = compose(
	withProps({
		googleMapURL: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAWLQgl-Pv6qz5yrQ6YpMe8Zqkyzebh6cs',
		loadingElement: <div style={{ height: `100%` }} />,
		containerElement: <div style={{ height: `90vh` }} />,
		mapElement: <div style={{ height: `100%` }} />
	}),
	withStateHandlers(
		() => ({
			isOpen: false,
			mapId: null
		}),
		{
			onToggleOpen: ({ isOpen }) => () => ({
				isOpen: !isOpen
			}),
			changeMapid: () => (mapId) => ({
				mapId: mapId
			})
		}
	),
	withScriptjs,
	withGoogleMap
)((props) => (
	<GoogleMap
		defaultZoom={props.zoom}
		defaultCenter={
			props.subCounties[0] == undefined ? (
				{
					lat: 1.2921,
					lng: 36.8219,
					latitudeDelta: 0.00922 * 1.5,
					longitudeDelta: 0.00421 * 1.5
				}
			) : (
				{
					lat: props.subCounties[0].lat,
					lng: props.subCounties[0].lon,
					latitudeDelta: props.subCounties[0].latDelta,
					longitudeDelta: props.subCounties[0].lonDelta
				}
			)
		}
		defaultOptions={{
			scrollwheel: false,
			zoomControl: true,
			styles: [
				{
					featureType: 'water',
					stylers: [ { saturation: 43 }, { lightness: -11 }, { hue: '#0088ff' } ]
				},
				{
					featureType: 'road',
					elementType: 'geometry.fill',
					stylers: [ { hue: '#ff0000' }, { saturation: -100 }, { lightness: 99 } ]
				},
				{
					featureType: 'road',
					elementType: 'geometry.stroke',
					stylers: [ { color: '#808080' }, { lightness: 54 } ]
				},
				{
					featureType: 'landscape.man_made',
					elementType: 'geometry.fill',
					stylers: [ { color: '#ece2d9' } ]
				},
				{
					featureType: 'poi.park',
					elementType: 'geometry.fill',
					stylers: [ { color: '#ccdca1' } ]
				},
				{
					featureType: 'road',
					elementType: 'labels.text.fill',
					stylers: [ { color: '#767676' } ]
				},
				{
					featureType: 'road',
					elementType: 'labels.text.stroke',
					stylers: [ { color: '#ffffff' } ]
				},
				{ featureType: 'poi', stylers: [ { visibility: 'off' } ] },
				{
					featureType: 'landscape.natural',
					elementType: 'geometry.fill',
					stylers: [ { visibility: 'on' }, { color: '#b8cb93' } ]
				},
				{ featureType: 'poi.park', stylers: [ { visibility: 'on' } ] },
				{
					featureType: 'poi.sports_complex',
					stylers: [ { visibility: 'on' } ]
				},
				{ featureType: 'poi.medical', stylers: [ { visibility: 'on' } ] },
				{
					featureType: 'poi.business',
					stylers: [ { visibility: 'simplified' } ]
				}
			]
		}}
	>
		{props.counties !== '' ? (
			props.counties.map((marker) => (
				<Marker
					position={{
						lat: marker.lat,
						lng: marker.lon,
						latitudeDelta: marker.latDelta,
						longitudeDelta: marker.lonDelta
					}}
					visible={true}
					clickable={true}
					title={marker.name}
					defaultTitle={marker.name}
					onClick={() => {
						props.marker(marker);
						props.onToggleOpen();
					}}
				>
					{props.id == marker.id && props.isOpen ? (
						<InfoBox
							onCloseClick={() => {
								props.onToggleOpen();
							}}
							options={{ closeBoxURL: ``, enableEventPropagation: true }}
						>
							<Card
								className="root"
								style={{
									height: 120,
									width: 120,
									backgroundColor: '#121212'
								}}
								onClick={() => {
									props.getSubcountiesData(marker.id);
									props.onToggleOpen();
								}}
							>
								<CardActionArea
									style={{
										height: 80,
										width: 100
									}}
								>
									<CardMedia
										className="media"
										image="https://i.pinimg.com/originals/15/39/47/1539471553f7ad85a6cbdd0a6534d00b.jpg"
										title="Contemplative Reptile"
										style={{
											height: 70,
											width: 120
										}}
									/>
									<CardContent>
										<Typography
											gutterBottom
											variant="h6"
											component="h7"
											style={{
												color: 'white'
											}}
										>
											{marker.name}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardActions
									style={{
										height: 10
									}}
								/>
							</Card>
						</InfoBox>
					) : (
						<div />
					)}
				</Marker>
			))
		) : (
			<div />
		)}
		{props.subCounties !== '' ? (
			props.subCounties.map((marker) => (
				<Marker
					position={{
						lat: marker.lat,
						lng: marker.lon,
						latitudeDelta: marker.latDelta,
						longitudeDelta: marker.lonDelta
					}}
					visible={true}
					clickable={true}
					title={marker.name}
					defaultTitle={marker.name}
					onClick={() => {
						props.marker(marker);
						props.onToggleOpen();
					}}
				>
					{props.id == marker.id && props.isOpen ? (
						<InfoBox
							onCloseClick={()=>{
								props.onToggleOpen();
							}}
							options={{ closeBoxURL: ``, enableEventPropagation: true }}
						>
							<Card
								className="root"
								style={{
									height: 120,
									width: 120,
									backgroundColor: '#121212'
								}}
								onClick={() => {
									props.getDevicesData(marker.id);
									props.onToggleOpen();
								}}
							>
								<CardActionArea
									style={{
										height: 80,
										width: 100
									}}
								>
									<CardMedia
										className="media"
										image="https://i.pinimg.com/originals/15/39/47/1539471553f7ad85a6cbdd0a6534d00b.jpg"
										title="Contemplative Reptile"
										style={{
											height: 70,
											width: 120
										}}
									/>
									<CardContent>
										<Typography
											gutterBottom
											variant="h6"
											component="h7"
											style={{
												color: 'white'
											}}
										>
											{marker.name}
										</Typography>
									</CardContent>
								</CardActionArea>
								<CardActions
									style={{
										height: 10
									}}
								/>
							</Card>
						</InfoBox>
					) : (
						<div />
					)}
				</Marker>
			))
		) : (
			<div />
		)}
	</GoogleMap>
));
