import React from 'react'

import { connect } from 'react-redux';
import { Line } from 'react-chartjs-2';
import { getDevicesData } from '../Actions';
import { Container, Row, Col } from 'react-bootstrap';


class Graph extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isMarkerShown: false,
            deviceId: null,
            pageSize: 10,
            pageIndex: 0,
            deviceData: null
        };
    }

    componentDidMount() {
        this.props.getDevicesData({ page: this.state.pageIndex, items: this.state.pageSize, deviceId: this.props.location.state.deviceId })
    }

    render() {

        const { aqi25, aqi10, minutes, mqSensorCO } = this.props

        return (
            <Container>
                <Row>
                    <Col xs="4">
                        <Line
                            height={300}
                            data={{
                                labels: minutes,
                                datasets: [{
                                    label: 'Air quality for PMT25',
                                    data: aqi25,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            }}
                        />
                    </Col>
                    <Col xs="4">
                        <Line
                            height={300}
                            data={{
                                labels: minutes,
                                datasets: [{
                                    label: 'Air quality for PMT10',
                                    data: aqi10,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            }}
                        />
                    </Col>
                    <Col xs="4">
                        <Line
                            height={300}
                            data={{
                                labels: minutes,
                                datasets: [{
                                    label: 'Carbon Monoxide Density Values',
                                    data: mqSensorCO,
                                    backgroundColor: [
                                        'rgba(255, 99, 132, 0.2)',
                                        'rgba(54, 162, 235, 0.2)',
                                        'rgba(255, 206, 86, 0.2)',
                                        'rgba(75, 192, 192, 0.2)',
                                        'rgba(153, 102, 255, 0.2)',
                                        'rgba(255, 159, 64, 0.2)'
                                    ],
                                    borderColor: [
                                        'rgba(255, 99, 132, 1)',
                                        'rgba(54, 162, 235, 1)',
                                        'rgba(255, 206, 86, 1)',
                                        'rgba(75, 192, 192, 1)',
                                        'rgba(153, 102, 255, 1)',
                                        'rgba(255, 159, 64, 1)'
                                    ],
                                    borderWidth: 1
                                }]
                            }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <img src="https://miro.medium.com/max/1400/1*-zoAVf6ye_xy7k4H9XEFPg.png" height={300}></img>
                    </Col>
                </Row>
            </Container>
        )
    }
};

const mapStateToProps = ({ devicesData }) => {
    const { deviceData, aqi25, aqi10, minutes, mqSensorCO } = devicesData;
    return {
        aqi25, aqi10, minutes, mqSensorCO
    };
};

export default connect(mapStateToProps, { getDevicesData })(Graph);