import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';

import 'bootstrap/dist/css/bootstrap.min.css';

function CountyCreate(props) {
	const { onHide, createCounty } = props;
	const [code, setCode]= useState();
	const [name, setName]= useState();
	const [lat, setLat]= useState();
	const [long, setLong]= useState();
	return (
		<Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title id="contained-modal-title-vcenter">Add County</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form.Group>
					<Form.Label>Id</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Code" onChange={(event)=>{setCode(event.target.value)}} />
					<br />
					<Form.Label>Name</Form.Label>
					<Form.Control size="sm" type="text" placeholder="Name" onChange={(event)=>{setName(event.target.value)}} />
					<br />
					<Form.Label>Latitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Latitude" onChange={(event)=>{setLat(event.target.value)}} />
					<br />
					<Form.Label>Longitude</Form.Label>
					<Form.Control size="sm" type="number" placeholder="Longitude" onChange={(event)=>{setLong(event.target.value)}} />
					<br />
				</Form.Group>
			</Modal.Body>
			<Modal.Footer>
				<Button onClick={()=>{
					createCounty({ name: name, lat: lat, lon: long,  });
					onHide()
				}}>Submit</Button>
			</Modal.Footer>
		</Modal>
	);
}

export default CountyCreate;
