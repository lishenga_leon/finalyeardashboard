import React, { useReducer } from 'react';
import styled from 'styled-components';
import { useTable, usePagination } from 'react-table';
import DeleteForeverTwoToneIcon from '@material-ui/icons/DeleteForeverTwoTone';
import County from './County';
import CountyCreate from './CountyCreate';
import Delete from './Delete';
import EditIcon from '@material-ui/icons/Edit';
import { getCounties, updateCounty, createCounty } from '../../Actions';

const Styles = styled.div`
	padding: 1rem;

	table {
		border-spacing: 0;
		border: 1px solid black;

		tr {
			:last-child {
				td {
					border-bottom: 0;
				}
			}
		}

		th,
		td {
			margin: 0;
			padding: 0.5rem;
			border-bottom: 1px solid black;
			border-right: 1px solid black;

			:last-child {
				border-right: 0;
			}
		}
	}

	.pagination {
		padding: 0.5rem;
	}
`;

// Let's add a fetchData method to our Table component that will be used to fetch
// new data when pagination state changes
// We can also add a loading state to let our table know it's loading new data
function Table({
	columns,
	data,
	fetchData,
	loading,
	pageCount: controlledPageCount,
	setModalShow,
	setModalData,
	setModalDeleteShow,
	setModalDeleteData
}) {
	const {
		getTableProps,
		getTableBodyProps,
		headerGroups,
		prepareRow,
		page,
		canPreviousPage,
		canNextPage,
		pageOptions,
		pageCount,
		gotoPage,
		nextPage,
		previousPage,
		setPageSize,
		// Get the state from the instance
		state: { pageIndex, pageSize }
	} = useTable(
		{
			columns,
			data,
			initialState: { pageIndex: 0 }, // Pass our hoisted table state
			manualPagination: true, // Tell the usePagination
			// hook that we'll handle our own data fetching
			// This means we'll also have to provide our own
			// pageCount.
			pageCount: controlledPageCount
		},
		usePagination
	);

	// Listen for changes in pagination and use the state to fetch our new data
	React.useEffect(
		() => {
			fetchData({ pageIndex, pageSize });
		},
		[fetchData, pageIndex, pageSize]
	);

	// Render the UI for your table
	return (
		<div>
			<table {...getTableProps()}>
				<thead>
					{headerGroups.map((headerGroup) => (
						<tr {...headerGroup.getHeaderGroupProps()}>
							{headerGroup.headers.map((column) => (
								<th {...column.getHeaderProps()}>
									{column.render('Header')}
									<span>{column.isSorted ? column.isSortedDesc ? ' 🔽' : ' 🔼' : ''}</span>
								</th>
							))}
						</tr>
					))}
				</thead>
				<tbody {...getTableBodyProps()}>
					{page.map((row, i) => {
						prepareRow(row);
						return (
							<tr {...row.getRowProps()}>
								{row.cells.map((cell) => {
									if (cell.column.Header == 'Update') {
										return (
											<td>
												<EditIcon
													style={{
														alignItems: 'center',
														alignContent: 'center',
														paddingLeft: '10px'
													}}
													onClick={() => {
														setModalData(cell.row.original);
														setModalShow(true);
													}}
												/>
											</td>
										);
									} else if (cell.column.Header == 'Delete') {
										return (
											<td>
												<DeleteForeverTwoToneIcon
													style={{
														alignItems: 'center',
														alignContent: 'center',
														paddingLeft: '10px'
													}}
													onClick={() => {
														setModalDeleteData(cell.row.original);
														setModalDeleteShow(true);
													}}
												/>
											</td>
										);
									} else {
										return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
									}
								})}
							</tr>
						);
					})}
					<tr>
						{loading ? (
							// Use our custom loading state to show a loading indicator
							<td colSpan="10000">Loading...</td>
						) : (
								<td colSpan="10000">
									Showing {page.length} of ~{data.length} results
								</td>
							)}
					</tr>
				</tbody>
			</table>
			{/* 
        Pagination can be built however you'd like. 
        This is just a very basic UI implementation:
      */}
			<div className="pagination">
				<button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
					{'<<'}
				</button>{' '}
				<button onClick={() => previousPage()} disabled={!canPreviousPage}>
					{'<'}
				</button>{' '}
				<button onClick={() => nextPage()} disabled={!canNextPage}>
					{'>'}
				</button>{' '}
				<button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
					{'>>'}
				</button>{' '}
				<span>
					Page{' '}
					<strong>
						{pageIndex + 1} of {pageOptions.length}
					</strong>{' '}
				</span>
				<span>
					| Go to page:{' '}
					<input
						type="number"
						defaultValue={pageIndex + 1}
						onChange={(e) => {
							const page = e.target.value ? Number(e.target.value) - 1 : 0;
							gotoPage(page);
						}}
						style={{ width: '100px' }}
					/>
				</span>{' '}
				<select
					value={pageSize}
					onChange={(e) => {
						setPageSize(Number(e.target.value));
					}}
				>
					{[10, 20, 30, 40, 50].map((pageSize) => (
						<option key={pageSize} value={pageSize}>
							Show {pageSize}
						</option>
					))}
				</select>
			</div>
		</div>
	);
}

function Counties() {
	const columns = React.useMemo(
		() => [
			{
				Header: 'Info',
				columns: [
					{
						Header: 'id',
						accessor: 'id'
					},
					{
						Header: 'Name',
						accessor: 'name'
					},
					{
						Header: 'Latitude',
						accessor: 'lat'
					},
					{
						Header: 'Longitude',
						accessor: 'lon'
					}
				]
			},
			{
				Header: 'Details',
				columns: [
					{
						Header: 'Created At',
						accessor: 'createdAt'
					},
					{
						Header: 'Updated At',
						accessor: 'updatedAt'
					}
				]
			},
			{
				Header: 'Action',
				columns: [
					{
						Header: 'Update',
						accessor: 'up'
					},
					{
						Header: 'Delete',
						accessor: 'down'
					}
				]
			}
		],
		[]
	);

	// We'll start our table without any data
	const [data, setData] = React.useState([]);
	const [loading, setLoading] = React.useState(false);
	const [pageCount, setPageCount] = React.useState(20);
	const fetchIdRef = React.useRef(0);
	const [modalShow, setModalShow] = React.useState(false);
	const [createShow, setCreateShow] = React.useState(false);
	const [modalDeleteShow, setModalDeleteShow] = React.useState(false);
	const [modalData, setModalData] = React.useState();
	const [modalDeleteData, setModalDeleteData] = React.useState();

	const fetchData = React.useCallback(({ pageSize, pageIndex }) => {
		// This will get called when the table needs new data
		// You could fetch your data from literally anywhere,
		// even a server. But for this example, we'll just fake it.

		// Give this fetch an ID
		const fetchId = ++fetchIdRef.current;
		// Set the loading state
		setLoading(true);

		// We'll even set a delay to simulate a server here
		setTimeout(() => {
			// Only update the data if this is the latest fetch
			if (fetchId === fetchIdRef.current) {
				getCounties({ page: pageIndex, items: pageSize, setData: setData });
				setPageCount(20);
				setLoading(false);
			}
		}, 1000);
	}, []);

	return (
		<Styles>
			<div style={{ display: "flex" }}>
				<button
					style={{ marginLeft: "auto" }}
					onClick={()=>{
						setCreateShow(true)
					}}
				>
					Add County
			</button>
			</div>
			<CountyCreate show={createShow} onHide={() => setCreateShow(false)} createCounty={createCounty} />
			{modalData != null ? <County data={modalData} show={modalShow} onHide={() => setModalShow(false)} updateCounty={updateCounty} /> : null}
			{modalDeleteData != null ? (
				<Delete data={modalDeleteData} show={modalDeleteShow} onHide={() => setModalDeleteShow(false)} />
			) : null}
			<Table
				columns={columns}
				data={data}
				fetchData={fetchData}
				loading={loading}
				pageCount={pageCount}
				setModalShow={setModalShow}
				setModalData={setModalData}
				setModalDeleteShow={setModalDeleteShow}
				setModalDeleteData={setModalDeleteData}
			/>
		</Styles>
	);
}

export default Counties;