import Dashboard from "@material-ui/icons/Dashboard";
import LocationOn from "@material-ui/icons/LocationOn";
import Maps from "views/Maps/Maps.js";
import Graph from "views/Graph.js";
import Counties from "views/Counties/Counties.js";
import Subcounties from "views/Subcounties/Subcounties.js";
import Devices from "views/Devices/Devices.js";
import DevicesIcon from '@material-ui/icons/Devices';

const dashboardRoutes = [

  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "خرائط",
    icon: Dashboard,
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/counties",
    name: "Counties",
    rtlName: "خرائط",
    icon: LocationOn,
    component: Counties,
    layout: "/admin"
  },
  {
    path: "/subcounties",
    name: "Subounties",
    rtlName: "خرائط",
    icon: LocationOn,
    component: Subcounties,
    layout: "/admin"
  },
  {
    path: "/devices",
    name: "Devices",
    rtlName: "خرائط",
    icon: DevicesIcon,
    component: Devices,
    layout: "/admin"
  },
  {
    path: "/graph",
    name: "Graph",
    rtlName: "خرائط",
    icon: DevicesIcon,
    component: Graph,
    layout: "/admin"
  },
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   rtlName: "ملف تعريفي للمستخدم",
  //   icon: Person,
  //   component: UserProfile,
  //   layout: "/admin"
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/admin"
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin"
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/admin"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl"
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/admin"
  // }
];

export default dashboardRoutes;
