import { SUBCOUNTIES_FAIL, SUBCOUNTIES_SUCCESS, LOADING_STATE, LOADMORE } from './types';
import { apiIp } from '../Config';
import axios from 'axios';

// function to make API call to server to get subcounties
export const getSubcountiesServer = ({ page, items, loadmore, countyId, subCounties }) => {
	return (dispatch) => {
		if (loadmore) {
			dispatch({ type: LOADMORE, payload: true });
			const data = {
				page: page,
				items: items,
				countyId: countyId
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/Subcounty/getSubcountiesCounty', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						SubcountiesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						SubcountiesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						SubcountiesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => SubcountiesFail(dispatch, 'Failed: Kindly try again later'));
		} else {
			dispatch({ type: LOADING_STATE, payload: true });
			const data = {
				page: page,
				items: items,
				countyId: countyId
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/Subcounty/getSubcountiesCounty', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						SubcountiesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						SubcountiesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						SubcountiesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => SubcountiesFail(dispatch, 'Failed: Kindly try again later'));
		}
	};
};

const SubcountiesFail = (dispatch, error) => {
	dispatch({
		type: SUBCOUNTIES_FAIL,
		error: error
	});
};

const SubcountiesSuccess = (dispatch, data, page, items, { loadmore }) => {
	dispatch({
		type: SUBCOUNTIES_SUCCESS,
		payload: data,
		page: page,
		items: items,
		loadmore: loadmore
	});
};

// function to make API call to server to get subcounties
export const getSubcounties = ({ page, items, setData }) => {
	let final = [];
	const data = {
		page: page,
		items: items
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/Subcounty/getAllSubcounties', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return setData([]);
			} else if (JSON.stringify(response.status) == 200) {
				final = [ ...response.data.data ];
				return setData(final);
			} else {
				return setData([]);
			}
		})
		.catch((error) => {
			return setData([]);
		});
};

// function to make API call to server to update subcounty
export const updateSubcounty = ({ subcountyId, countyId, name, lat, lon, latDelta, lonDelta }) => {
	const data = {
		subcountyId: subcountyId,
		countyId: countyId,
		name: name,
		lat: lat,
		lon: lon,
		latDelta: latDelta,
		lonDelta: lonDelta
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/Subcounty/updateSubcounty', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};

// function to make API call to server to create subcounty
export const createSubCounty = ({ countyId, name, lat, lon }) => {
	const data = {
		county: countyId,
		name: name,
		lat: lat,
		lon: lon,
		latDelta: 0,
		lonDelta: 0
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/Subcounty/registerSubcounty', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};
