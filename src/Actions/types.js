export const API_IP = 'http://142.93.7.234:84/api/'
export const TOAST_DISPLAY= 'toast_display'
export const TOAST_CLEAR= 'toast_clear'
export const SPINNER_CLEAR= 'spinner_clear'
export const ERROR_CLEAR= 'error_clear'
export const EMAIL_CLEAR= 'email_clear'
export const INCORRECT_PASSWORD='incorrect_password'
export const TRY_AGAIN='try_again'
export const LOADING_STATE = 'loading_clear';
export const REFRESH_CONTROL_STATE = 'control';
export const RECALLER_STATE = 'control_state';
export const LOADMORE = 'state_load';


//Counties Component
export const COUNTIES_SUCCESS = 'counties_success'
export const COUNTIES_FAIL = 'counties_fail'

//Subcounties Component
export const SUBCOUNTIES_SUCCESS = 'subcounties_success'
export const SUBCOUNTIES_FAIL = 'subcounties_fail'

//Devices Component
export const DEVICES_SUCCESS = 'devices_success'
export const DEVICES_FAIL = 'devices_fail'

//Devicesdata Component
export const DEVICESDATA_SUCCESS = 'devicesdata_success'
export const DEVICESDATA_FAIL = 'devicesdata_fail'
export const DEVICESDATA_GOTTEN = 'devicesdata_gotten'
export const DEVICESDATA_NOT = 'devicesdata_not'
