import { COUNTIES_FAIL, COUNTIES_SUCCESS, LOADING_STATE, LOADMORE } from './types';
import { apiIp } from '../Config';
import axios from 'axios';

// function to make API call to server to get counties
export const getCountiesServer = ({ page, items, loadmore, counties }) => {
	return (dispatch) => {
		if (loadmore) {
			dispatch({ type: LOADMORE, payload: true });
			const data = {
				page: page,
				items: items
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/county/getAllCounties', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						CountiesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						CountiesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						CountiesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => CountiesFail(dispatch, 'Failed: Kindly try again later'));
		} else {
			dispatch({ type: LOADING_STATE, payload: true });
			const data = {
				page: 0,
				items: 47
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/county/getAllCounties', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						CountiesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						CountiesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						CountiesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => CountiesFail(dispatch, 'Failed: Kindly try again later'));
		}
	};
};

const CountiesFail = (dispatch, error) => {
	dispatch({
		type: COUNTIES_FAIL,
		error: error
	});
};

const CountiesSuccess = (dispatch, data, page, items, { loadmore }) => {
	dispatch({
		type: COUNTIES_SUCCESS,
		payload: data,
		page: page,
		items: items,
		loadmore: loadmore
	});
};

// function to make API call to server to get counties
export const getCounties = ({ page, items, setData }) => {
	let final = [];
	const data = {
		page: page,
		items: items
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/county/getAllCounties', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return setData([]);
			} else if (JSON.stringify(response.status) == 200) {
				final = [ ...response.data.data ];
				return setData(final);
			} else {
				return setData([]);
			}
		})
		.catch((error) => {
			return setData([]);
		});
};

// function to make API call to server to update county
export const updateCounty = ({ countyId, name, lat, lon, latDelta, lonDelta }) => {
	const data = {
		countyId: countyId,
		name: name,
		lat: lat,
		lon: lon,
		latDelta: latDelta,
		lonDelta: lonDelta
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/county/updateCounty', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};

// function to make API call to server to create county
export const createCounty = ({ name, lat, lon }) => {
	const data = {
		name: name,
		lat: lat,
		lon: lon,
		latDelta: 0,
		lonDelta: 0
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/county/registerCounty', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};
