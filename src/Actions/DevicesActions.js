import { DEVICES_FAIL, DEVICES_SUCCESS, LOADING_STATE, LOADMORE, DEVICESDATA_GOTTEN, DEVICESDATA_NOT } from './types';
import { apiIp } from '../Config';
import axios from 'axios';
import _ from 'lodash';

// function to make API call to server to get devices
export const getDevicesServer = ({ page, items, loadmore, subcountyId }) => {
	return (dispatch) => {
		if (loadmore) {
			dispatch({ type: LOADMORE, payload: true });
			const data = {
				page: page,
				items: items,
				subcountyId: subcountyId
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/device/getSubcountyDevices', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						DevicesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						DevicesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						DevicesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => DevicesFail(dispatch, 'Failed: Kindly try again later'));
		} else {
			dispatch({ type: LOADING_STATE, payload: true });
			const data = {
				page: page,
				items: items,
				subcountyId: subcountyId
			};
			let headers = {
				accept: 'application/json',
				'Accept-Language': 'en-US,en;q=0.8',
				'Content-Type': 'application/json'
			};
			axios
				.post(apiIp + 'api/device/getSubcountyDevices', data, { headers: headers })
				.then((response) => {
					if (JSON.stringify(response.status) == 500) {
						DevicesFail(dispatch, 'Failed: Kindly try again later');
					} else if (JSON.stringify(response.status) == 200) {
						DevicesSuccess(dispatch, response.data.data, page, items, { loadmore: false });
					} else {
						DevicesFail(dispatch, 'Session Expired: Kindly login again');
					}
				})
				.catch((error) => console.log(error));
		}
	};
};

const DevicesFail = (dispatch, error) => {
	dispatch({
		type: DEVICES_FAIL,
		error: error
	});
};

const DevicesSuccess = (dispatch, data, page, items, { loadmore }) => {
	dispatch({
		type: DEVICES_SUCCESS,
		payload: data,
		page: page,
		items: items,
		loadmore: loadmore
	});
};

// function to make API call to server to get subcounties
export const getDevices = ({ page, items, setData }) => {
	let final = [];
	const data = {
		page: page,
		items: items
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/device/getAllDevices', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return setData([]);
			} else if (JSON.stringify(response.status) == 200) {
				final = [...response.data.data];
				return setData(final);
			} else {
				return setData([]);
			}
		})
		.catch((error) => {
			return setData([]);
		});
};

// function to make API call to server to update subcounty
export const updateDevice = ({ deviceId, subcounty, serialNo, lat, lon, latDelta, lonDelta }) => {
	const data = {
		deviceId: deviceId,
		subcounty: subcounty,
		serialNo: serialNo,
		lat: lat,
		lon: lon,
		latDelta: latDelta,
		lonDelta: lonDelta
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	axios
		.post(apiIp + 'api/Subcounty/updateSubcounty', data, { headers: headers })
		.then((response) => {
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};

// function to make API call to server to get subcounties
export const getDevicesData = ({ page, items, deviceId }) => {
	return (dispatch) => {
		const data = {
			deviceId: deviceId,
			page: page,
			items: 1000
		};
		let headers = {
			accept: 'application/json',
			'Accept-Language': 'en-US,en;q=0.8',
			'Content-Type': 'application/json'
		};
		axios
			.post(apiIp + 'api/devicedata/getParticularDeviceData', data, { headers: headers })
			.then((response) => {

				if (JSON.stringify(response.status) == 500) {

					return DevicesNot(dispatch, 'Session Expired: Kindly login again');

				} else if (JSON.stringify(response.status) == 200) {

					let final = [...response.data.data]

					let aqi25 = _.map(final, function (data) {
						return data.aqi25;
					})
			
					let aqi10 = _.map(final, function (data) {
						return data.aqi10;
					})
			
					let minutes = _.map(final, function (data) {
						let d = new Date(data.createdAt)
						return d.getMinutes();
					})
			
					let mqSensorCO = _.map(final, function (data) {
						return data.mqSensorCO;
					})

					return DevicesGotten(dispatch, aqi25, aqi10, minutes, mqSensorCO);
				} else {
					return DevicesNot(dispatch, 'Session Expired: Kindly login again');
				}
			})
			.catch((error) => {
				return DevicesNot(dispatch, 'Session Expired: Kindly login again');
			});
	}
};


const DevicesNot = (dispatch, error) => {
	dispatch({
		type: DEVICESDATA_NOT,
		error: error
	});
};

const DevicesGotten = (dispatch, aqi25, aqi10, minutes, mqSensorCO) => {
	dispatch({
		type: DEVICESDATA_GOTTEN,
		aqi25: aqi25, aqi10: aqi10, minutes: minutes, mqSensorCO: mqSensorCO
	});
};

// function to make API call to server to create device
export const createDevice = ({ subcountyId, serialNo, lat, lon }) => {
	const data = {
		subcounty: subcountyId,
		serialNo: serialNo,
		lat: lat,
		lon: lon,
		latDelta: 0,
		lonDelta: 0
	};
	let headers = {
		accept: 'application/json',
		'Accept-Language': 'en-US,en;q=0.8',
		'Content-Type': 'application/json'
	};
	console.log(data)
	axios
		.post(apiIp + 'api/device/registerDevice', data, { headers: headers })
		.then((response) => {
			console.log(response)
			if (JSON.stringify(response.status) == 500) {
				return;
			} else if (JSON.stringify(response.status) == 200) {
				return;
			} else {
				return;
			}
		})
		.catch((error) => {
			return;
		});
};
